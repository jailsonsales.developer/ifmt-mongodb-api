sshpass -e ssh -o StrictHostKeyChecking=no $SSH_USER@$SSH_HOST << ENDSSH
  docker network create iftmt_mongo_network_mongo || true
  docker pull $IMAGEM_PRONTA
  docker rm --force mongo_ci_cd_api || true
  docker run -d -it -p 8081:8081 --network iftmt_mongo_network_mongo -e URL_BANCO=mongodb://mongo:27017 --name mongo_ci_cd_api $IMAGEM_PRONTA
  docker system prune -f
ENDSSH

