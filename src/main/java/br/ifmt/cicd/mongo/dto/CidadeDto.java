package br.ifmt.cicd.mongo.dto;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class CidadeDto {
    @Schema(example = "Cuiabá")
    private String nome;
    @Schema(example = "MT")
    private String estado;
}
