package br.ifmt.cicd.mongo.dto;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.QueryParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaParamsDto {

    @QueryParam("nome")
    private String nome;

    @QueryParam("cpf")
    private String cpf;

    @QueryParam("cidade")
    private String cidade;

    @QueryParam("estado")
    private String estado;

    @QueryParam("page")
    @DefaultValue("0")
    private Integer page;

    @QueryParam("size")
    @DefaultValue("10")
    private Integer size;
}
