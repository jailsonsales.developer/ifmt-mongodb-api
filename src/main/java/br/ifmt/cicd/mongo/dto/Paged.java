package br.ifmt.cicd.mongo.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Paged<T> {
    private long count;
    private int page;
    private int pageSize;
    private int totalPages;
    private List<T> content;
}
