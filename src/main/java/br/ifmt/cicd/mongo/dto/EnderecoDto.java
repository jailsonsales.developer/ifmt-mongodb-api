package br.ifmt.cicd.mongo.dto;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


@Getter
@Setter
public class EnderecoDto {

    @Schema(example = "Rua abc")
    private String logradouro;

    @Schema(example = "238")
    private String numero;

    @Schema(example = "Próximo a rotatória")
    private String complemento;

    @Schema(example = "Centro")
    private String bairro;

    private CidadeDto cidade;
}
