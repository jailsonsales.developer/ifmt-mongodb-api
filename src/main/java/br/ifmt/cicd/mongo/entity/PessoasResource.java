package br.ifmt.cicd.mongo.entity;

import br.ifmt.cicd.mongo.dto.ConsultaParamsDto;
import br.ifmt.cicd.mongo.dto.Paged;
import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
import io.quarkus.mongodb.panache.PanacheQuery;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.bson.types.ObjectId;

import java.time.LocalDateTime;

@Path("pessoas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PessoasResource {

    @GET
    @Path("/{id}")
    public Pessoa findById(@PathParam("id") String id) {
        return Pessoa.findById(new ObjectId(id));
    }

    @POST
    public Response salvar(Pessoa pessoa) {
        pessoa.setDataHoraCadastro(LocalDateTime.now());
        pessoa.persist();
        return Response.status(201).build();
    }

    @PUT
    @Path("/{id}")
    public void atualizar(@PathParam("id") String id, Pessoa pessoa) {
        pessoa.setId(new ObjectId(id));
        pessoa.update();
    }

    @DELETE
    @Path("/{id}")
    public void excluir(@PathParam("id") String id) {
        Pessoa pessoa = Pessoa.findById(new ObjectId(id));
        pessoa.delete();
    }

    @GET
    public Paged<Pessoa> buscar(@BeanParam ConsultaParamsDto params) {
        PanacheQuery<PanacheMongoEntityBase> panacheQuery = Pessoa.findByParams(params);
        return Paged.<Pessoa>builder()
                .pageSize(params.getSize())
                .page(params.getPage())
                .totalPages(panacheQuery.pageCount())
                .count(panacheQuery.count())
                .content(panacheQuery.list())
                .build();
    }

    @DELETE
    public void deleteAll() {
        Pessoa.deleteAll();
    }
}
