package br.ifmt.cicd.mongo.entity;

import br.ifmt.cicd.mongo.dto.ConsultaParamsDto;
import br.ifmt.cicd.mongo.dto.EnderecoDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
import io.quarkus.mongodb.panache.PanacheQuery;
import io.quarkus.mongodb.panache.common.MongoEntity;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@MongoEntity(collection = "pessoas")
@Getter
@Setter
public class Pessoa extends PanacheMongoEntityBase {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private ObjectId id;

    @Schema(example = "Eduardo Anthony Rodrigo Ribeiro")
    private String nome;

    @Schema(example = "(82) 3746-5855")
    private String telefone;

    private LocalDate dataNascimento;

    private LocalDateTime dataHoraCadastro;

    @Schema(example = "11.129.335-2")
    private String cpf;

    private List<EnderecoDto> enderecos;

    public static PanacheQuery<PanacheMongoEntityBase> findByParams(ConsultaParamsDto params) {
        StringBuilder queryAux = new StringBuilder();
        Parameters parameters = new Parameters();
        if (StringUtils.isNotBlank(params.getNome())) {
            parameters.and("nome", params.getNome());
            queryAux.append(",'nome': {'$regex': :nome, '$options': 'i'}");
        }
        if (StringUtils.isNotBlank(params.getCpf())) {
            parameters.and("cpf", params.getCpf());
            queryAux.append(",'cpf': {'$regex': :cpf}");
        }
        if (StringUtils.isNotBlank(params.getCidade())) {
            parameters.and("cidade", params.getCidade());
            queryAux.append(",'enderecos.cidade.nome': {'$regex': :cidade, '$options': 'i'}");
        }
        if (StringUtils.isNotBlank(params.getEstado())) {
            parameters.and("estado", params.getEstado());
            queryAux.append(",'enderecos.cidade.estado': :estado");
        }
        if (queryAux.length() > 0)
            queryAux.deleteCharAt(0);
        return find("{" + queryAux + "}", Sort.descending("dataHoraCadastro"), parameters).page(params.getPage(), params.getSize());
    }
}
